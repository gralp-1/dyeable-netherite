![header](https://nblr.cc/8tren)

# Default Dark Mode
## Resource Pack for Minecraft: Java Edition

[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-brightgreen.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
[![Release](https://img.shields.io/github/v/release/xnebulr/Minecraft-Default-Dark-Mode?label=Release&color=brightgreen&cacheSeconds=3600)](https://github.com/xnebulr/Minecraft-Default-Dark-Mode/releases/latest)
[![GitHub Downloads](https://img.shields.io/github/downloads/xnebulr/Minecraft-Default-Dark-Mode/total?label=Downloads&logo=github&cacheSeconds=3600)](https://github.com/xnebulr/Minecraft-Default-Dark-Mode/releases)
[![CurseForge Downloads](https://img.shields.io/endpoint?url=https://api.ddm.nebulr.dev/shields/downloads/curseforge)](https://www.curseforge.com/minecraft/texture-packs/default-dark-mode/files)
[![Planet Minecraft Downloads](https://img.shields.io/endpoint?url=https://api.ddm.nebulr.dev/shields/downloads/planetminecraft)](https://www.planetminecraft.com/texture-pack/default-dark-mode)

This Resource Pack is a simple dark mode / dark theme for the Minecraft GUI. It uses only modified vanilla textures.

## Alternative Downloads

* [Planet Minecraft](https://www.planetminecraft.com/texture_pack/default-dark-mode/)
* [CurseForge](https://www.curseforge.com/minecraft/texture-packs/default-dark-mode)

## Screenshots

![screenshot](https://nblr.cc/w7hqb)

![screenshot](https://nblr.cc/byr6t)

![screenshot](https://nblr.cc/e9qkr)

![screenshot](https://nblr.cc/uf8jp)

![screenshot](https://nblr.cc/akdp6)

![screenshot](https://nblr.cc/3xmn4)

![screenshot](https://nblr.cc/pn9xt)